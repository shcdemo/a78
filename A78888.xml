<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A78888">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The Kings message to both Houses January 12. 1641.</title>
    <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A78888 of text R24816 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.3[34]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A78888</idno>
    <idno type="STC">Wing C2449</idno>
    <idno type="STC">Thomason 669.f.3[34]</idno>
    <idno type="STC">ESTC R24816</idno>
    <idno type="EEBO-CITATION">99872071</idno>
    <idno type="PROQUEST">99872071</idno>
    <idno type="VID">160592</idno>
    <idno type="PROQUESTGOID">2248513096</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A78888)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160592)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f3[34])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The Kings message to both Houses January 12. 1641.</title>
      <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
      <author>Charles I, King of England, 1600-1649.</author>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by Robert Barker, printer to the Kings most Excellent Majesty: and by the assignes of John Bill,</publisher>
      <pubPlace>Imprinted at London :</pubPlace>
      <date>1641 [i.e. 1642]</date>
     </publicationStmt>
     <notesStmt>
      <note>"His Majesties profession and addition to his last message to the Parliament" is dated: January 14, 1641 [i.e. 1642].</note>
      <note>With engraving of royal seal of Charles I at head of document.</note>
      <note>Reproductions of the originals in the British Library and the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1625-1649 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A78888</ep:tcp>
    <ep:estc> R24816</ep:estc>
    <ep:stc> (Thomason 669.f.3[34]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>The Kings message to both Houses January 12. 1641.</ep:title>
    <ep:author>England and Wales. Sovereign</ep:author>
    <ep:publicationYear>1642</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>437</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-08</date><label>John Pas</label>
        Sampled and proofread
      </change>
   <change><date>2008-08</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A78888-e10">
  <body xml:id="A78888-e20">
   <pb facs="tcp:160592:1" rend="simple:additions" xml:id="A78888-001-a"/>
   <div type="text" xml:id="A78888-e30">
    <head xml:id="A78888-e40">
     <w lemma="❧" pos="sy" xml:id="A78888-001-a-0010">❧</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-0020">The</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A78888-001-a-0030">Kings</w>
     <w lemma="message" pos="n1" xml:id="A78888-001-a-0040">Message</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-0050">to</w>
     <w lemma="both" pos="d" xml:id="A78888-001-a-0060">both</w>
     <w lemma="house" pos="n2" xml:id="A78888-001-a-0070">Houses</w>
     <pc unit="sentence" xml:id="A78888-001-a-0080">.</pc>
     <w lemma="January" pos="nn1" xml:id="A78888-001-a-0090">January</w>
     <w lemma="12." pos="crd" xml:id="A78888-001-a-0100">12.</w>
     <w lemma="1641." pos="crd" xml:id="A78888-001-a-0110">1641.</w>
     <pc unit="sentence" xml:id="A78888-001-a-0120"/>
    </head>
    <p xml:id="A78888-e50">
     <hi xml:id="A78888-e60">
      <w lemma="hhis" pos="nng1" reg="Hhis'" rend="decorinit" xml:id="A78888-001-a-0130">HHis</w>
      <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78888-001-a-0140">Majestie</w>
      <w lemma="take" pos="vvg" xml:id="A78888-001-a-0150">taking</w>
      <w lemma="notice" pos="n1" xml:id="A78888-001-a-0160">notice</w>
      <w lemma="that" pos="cs" xml:id="A78888-001-a-0170">that</w>
      <w lemma="some" pos="d" xml:id="A78888-001-a-0180">some</w>
      <w lemma="conceive" pos="vvb" xml:id="A78888-001-a-0190">conceive</w>
      <w lemma="it" pos="pn" xml:id="A78888-001-a-0200">it</w>
      <w lemma="disputable" pos="j" xml:id="A78888-001-a-0210">disputable</w>
      <w lemma="whether" pos="cs" xml:id="A78888-001-a-0220">whether</w>
     </hi>
     <w lemma="his" pos="po" xml:id="A78888-001-a-0230">His</w>
     <w lemma="proceed" pos="n2-vg" xml:id="A78888-001-a-0240">proceedings</w>
     <w lemma="against" pos="acp" xml:id="A78888-001-a-0250">against</w>
     <w lemma="my" pos="po" xml:id="A78888-001-a-0260">my</w>
     <w lemma="lord" pos="n1" xml:id="A78888-001-a-0270">Lord</w>
     <hi xml:id="A78888-e70">
      <w lemma="Kymbolton" pos="nn1" xml:id="A78888-001-a-0280">Kymbolton</w>
      <pc xml:id="A78888-001-a-0290">,</pc>
     </hi>
     <w lemma="master" pos="n1" xml:id="A78888-001-a-0300">Master</w>
     <hi xml:id="A78888-e80">
      <w lemma="hollis" pos="nn1" xml:id="A78888-001-a-0310">Hollis</w>
      <pc xml:id="A78888-001-a-0320">,</pc>
     </hi>
     <w lemma="sir" pos="n1" xml:id="A78888-001-a-0330">Sir</w>
     <hi xml:id="A78888-e90">
      <w lemma="Arthur" pos="nn1" xml:id="A78888-001-a-0340">Arthur</w>
      <w lemma="Haslerig" pos="nn1" xml:id="A78888-001-a-0350">Haslerig</w>
      <pc xml:id="A78888-001-a-0360">,</pc>
     </hi>
     <w lemma="master" pos="n1" xml:id="A78888-001-a-0370">Master</w>
     <hi xml:id="A78888-e100">
      <w lemma="Pym" pos="nn1" xml:id="A78888-001-a-0380">Pym</w>
      <pc xml:id="A78888-001-a-0390">,</pc>
     </hi>
     <w lemma="master" pos="n1" xml:id="A78888-001-a-0400">Master</w>
     <hi xml:id="A78888-e110">
      <w lemma="hampden" pos="nn1" xml:id="A78888-001-a-0410">Hampden</w>
      <pc xml:id="A78888-001-a-0420">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-0430">and</w>
     <w lemma="master" pos="n1" xml:id="A78888-001-a-0440">Master</w>
     <hi xml:id="A78888-e120">
      <w lemma="Strode" pos="nn1" xml:id="A78888-001-a-0450">Strode</w>
      <pc xml:id="A78888-001-a-0460">,</pc>
     </hi>
     <w lemma="be" pos="vvb" xml:id="A78888-001-a-0470">be</w>
     <w lemma="legal" pos="j" reg="legal" xml:id="A78888-001-a-0480">legall</w>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-0490">and</w>
     <w lemma="agreeable" pos="j" xml:id="A78888-001-a-0500">agreeable</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-0510">to</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-0520">the</w>
     <w lemma="privilege" pos="n2" reg="privileges" xml:id="A78888-001-a-0530">Priviledges</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-0540">of</w>
     <w lemma="parliament" pos="n1" xml:id="A78888-001-a-0550">Parliament</w>
     <pc xml:id="A78888-001-a-0560">,</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-0570">and</w>
     <w lemma="be" pos="vvg" xml:id="A78888-001-a-0580">being</w>
     <w lemma="very" pos="av" xml:id="A78888-001-a-0590">very</w>
     <w lemma="desirous" pos="j" xml:id="A78888-001-a-0600">desirous</w>
     <w lemma="to" pos="prt" xml:id="A78888-001-a-0610">to</w>
     <w lemma="give" pos="vvi" xml:id="A78888-001-a-0620">give</w>
     <w lemma="satisfaction" pos="n1" xml:id="A78888-001-a-0630">satisfaction</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-0640">to</w>
     <w lemma="all" pos="d" xml:id="A78888-001-a-0650">all</w>
     <w lemma="man" pos="n2" xml:id="A78888-001-a-0660">men</w>
     <w lemma="in" pos="acp" xml:id="A78888-001-a-0670">in</w>
     <w lemma="all" pos="d" xml:id="A78888-001-a-0680">all</w>
     <w lemma="matter" pos="n2" xml:id="A78888-001-a-0690">matters</w>
     <w lemma="that" pos="cs" xml:id="A78888-001-a-0700">that</w>
     <w lemma="may" pos="vmb" xml:id="A78888-001-a-0710">may</w>
     <w lemma="seem" pos="vvi" xml:id="A78888-001-a-0720">seem</w>
     <w lemma="to" pos="prt" xml:id="A78888-001-a-0730">to</w>
     <w lemma="have" pos="vvi" xml:id="A78888-001-a-0740">have</w>
     <w lemma="relation" pos="n1" xml:id="A78888-001-a-0750">relation</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-0760">to</w>
     <w lemma="privilege" pos="n1" reg="privilege" xml:id="A78888-001-a-0770">Priviledge</w>
     <pc xml:id="A78888-001-a-0780">,</pc>
     <w lemma="be" pos="vvz" xml:id="A78888-001-a-0790">is</w>
     <w lemma="please" pos="vvn" xml:id="A78888-001-a-0800">pleased</w>
     <w lemma="to" pos="prt" xml:id="A78888-001-a-0810">to</w>
     <w lemma="wave" pos="vvi" xml:id="A78888-001-a-0820">wave</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-0830">His</w>
     <w lemma="former" pos="j" xml:id="A78888-001-a-0840">former</w>
     <w lemma="proceed" pos="n2-vg" xml:id="A78888-001-a-0850">Proceedings</w>
     <pc xml:id="A78888-001-a-0860">:</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-0870">and</w>
     <w lemma="all" pos="d" xml:id="A78888-001-a-0880">all</w>
     <w lemma="doubt" pos="n2" xml:id="A78888-001-a-0890">doubts</w>
     <w lemma="by" pos="acp" xml:id="A78888-001-a-0900">by</w>
     <w lemma="this" pos="d" xml:id="A78888-001-a-0910">this</w>
     <w lemma="mean" pos="n2" xml:id="A78888-001-a-0920">means</w>
     <w lemma="be" pos="vvg" xml:id="A78888-001-a-0930">being</w>
     <w lemma="settle" pos="vvn" reg="settled" xml:id="A78888-001-a-0940">setled</w>
     <pc xml:id="A78888-001-a-0950">,</pc>
     <w lemma="when" pos="crq" xml:id="A78888-001-a-0960">when</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-0970">the</w>
     <w lemma="mind" pos="n2" reg="minds" xml:id="A78888-001-a-0980">mindes</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-0990">of</w>
     <w lemma="man" pos="n2" xml:id="A78888-001-a-1000">men</w>
     <w lemma="be" pos="vvb" xml:id="A78888-001-a-1010">are</w>
     <w lemma="compose" pos="vvn" xml:id="A78888-001-a-1020">composed</w>
     <pc xml:id="A78888-001-a-1030">,</pc>
     <w lemma="his" pos="po" xml:id="A78888-001-a-1040">His</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78888-001-a-1050">Majestie</w>
     <w lemma="will" pos="vmb" xml:id="A78888-001-a-1060">will</w>
     <w lemma="proceed" pos="vvi" xml:id="A78888-001-a-1070">proceed</w>
     <w lemma="thereupon" pos="av" xml:id="A78888-001-a-1080">thereupon</w>
     <w lemma="in" pos="acp" xml:id="A78888-001-a-1090">in</w>
     <w lemma="a" pos="d" xml:id="A78888-001-a-1100">an</w>
     <w lemma="unquestionable" pos="j" xml:id="A78888-001-a-1110">unquestionable</w>
     <w lemma="way" pos="n1" xml:id="A78888-001-a-1120">way</w>
     <pc xml:id="A78888-001-a-1130">:</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-1140">And</w>
     <w lemma="assure" pos="vvz" xml:id="A78888-001-a-1150">assures</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-1160">His</w>
     <w lemma="parliament" pos="n1" xml:id="A78888-001-a-1170">Parliament</w>
     <w lemma="that" pos="cs" xml:id="A78888-001-a-1180">that</w>
     <w lemma="upon" pos="acp" xml:id="A78888-001-a-1190">upon</w>
     <w lemma="all" pos="d" xml:id="A78888-001-a-1200">all</w>
     <w lemma="occasion" pos="n2" xml:id="A78888-001-a-1210">occasions</w>
     <w lemma="he" pos="pns" xml:id="A78888-001-a-1220">He</w>
     <w lemma="will" pos="vmb" xml:id="A78888-001-a-1230">will</w>
     <w lemma="be" pos="vvi" xml:id="A78888-001-a-1240">be</w>
     <w lemma="as" pos="acp" xml:id="A78888-001-a-1250">as</w>
     <w lemma="careful" pos="j" reg="careful" xml:id="A78888-001-a-1260">carefull</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-1270">of</w>
     <w lemma="their" pos="po" xml:id="A78888-001-a-1280">their</w>
     <w lemma="privilege" pos="n2" reg="privileges" xml:id="A78888-001-a-1290">Priviledges</w>
     <pc xml:id="A78888-001-a-1300">,</pc>
     <w lemma="as" pos="acp" xml:id="A78888-001-a-1310">as</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-1320">of</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-1330">His</w>
     <w lemma="life" pos="n1" xml:id="A78888-001-a-1340">Life</w>
     <w lemma="or" pos="cc" xml:id="A78888-001-a-1350">or</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-1360">his</w>
     <w lemma="crown" pos="n1" xml:id="A78888-001-a-1370">Crown</w>
     <pc unit="sentence" xml:id="A78888-001-a-1380">.</pc>
    </p>
   </div>
   <div type="text" xml:id="A78888-e130">
    <head xml:id="A78888-e140">
     <w lemma="❧" pos="sy" xml:id="A78888-001-a-1390">❧</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-1400">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A78888-001-a-1410">Majesties</w>
     <w lemma="profession" pos="n1" xml:id="A78888-001-a-1420">Profession</w>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-1430">and</w>
     <w lemma="addition" pos="n1" xml:id="A78888-001-a-1440">Addition</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-1450">to</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-1460">His</w>
     <w lemma="last" pos="ord" xml:id="A78888-001-a-1470">last</w>
     <w lemma="message" pos="n1" xml:id="A78888-001-a-1480">Message</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-1490">to</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-1500">the</w>
     <w lemma="parliament" pos="n1" xml:id="A78888-001-a-1510">Parliament</w>
     <pc unit="sentence" xml:id="A78888-001-a-1520">.</pc>
     <w lemma="jan." pos="ab" xml:id="A78888-001-a-1530">Jan.</w>
     <w lemma="14." pos="crd" xml:id="A78888-001-a-1540">14.</w>
     <w lemma="1641." pos="crd" xml:id="A78888-001-a-1550">1641.</w>
     <pc unit="sentence" xml:id="A78888-001-a-1560"/>
    </head>
    <p xml:id="A78888-e150">
     <w lemma="hhs" pos="nng1" reg="Hhs'" rend="decorinit" xml:id="A78888-001-a-1570">HHs</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78888-001-a-1580">Majestie</w>
     <w lemma="be" pos="vvg" xml:id="A78888-001-a-1590">being</w>
     <w lemma="no" pos="dx" xml:id="A78888-001-a-1600">no</w>
     <w lemma="less" pos="avc-d" reg="less" xml:id="A78888-001-a-1610">lesse</w>
     <w lemma="tender" pos="j" xml:id="A78888-001-a-1620">tender</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-1630">of</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-1640">the</w>
     <w lemma="privilege" pos="n2" reg="privileges" xml:id="A78888-001-a-1650">Priviledges</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-1660">of</w>
     <w lemma="parliament" pos="n1" xml:id="A78888-001-a-1670">Parliament</w>
     <pc xml:id="A78888-001-a-1680">,</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-1690">and</w>
     <w lemma="think" pos="vvg" xml:id="A78888-001-a-1700">thinking</w>
     <w lemma="himself" pos="pr" xml:id="A78888-001-a-1710">himself</w>
     <w lemma="no" pos="dx" xml:id="A78888-001-a-1720">no</w>
     <w lemma="less" pos="avc-d" reg="less" xml:id="A78888-001-a-1730">lesse</w>
     <w lemma="concern" pos="vvn" xml:id="A78888-001-a-1740">concerned</w>
     <pc xml:id="A78888-001-a-1750">,</pc>
     <w lemma="that" pos="cs" xml:id="A78888-001-a-1760">that</w>
     <w lemma="they" pos="pns" xml:id="A78888-001-a-1770">they</w>
     <w lemma="be" pos="vvb" xml:id="A78888-001-a-1780">be</w>
     <w lemma="not" pos="xx" xml:id="A78888-001-a-1790">not</w>
     <w lemma="break" pos="vvn" xml:id="A78888-001-a-1800">broken</w>
     <pc xml:id="A78888-001-a-1810">,</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-1820">and</w>
     <w lemma="that" pos="cs" xml:id="A78888-001-a-1830">that</w>
     <w lemma="they" pos="pns" xml:id="A78888-001-a-1840">they</w>
     <w lemma="be" pos="vvb" xml:id="A78888-001-a-1850">be</w>
     <w lemma="assert" pos="vvn" xml:id="A78888-001-a-1860">asserted</w>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-1870">and</w>
     <w lemma="vindicate" pos="vvn" xml:id="A78888-001-a-1880">vindicated</w>
     <w lemma="whensoever" pos="crq" xml:id="A78888-001-a-1890">whensoever</w>
     <w lemma="they" pos="pns" xml:id="A78888-001-a-1900">they</w>
     <w lemma="be" pos="vvb" xml:id="A78888-001-a-1910">are</w>
     <w lemma="so" pos="av" xml:id="A78888-001-a-1920">so</w>
     <pc xml:id="A78888-001-a-1930">,</pc>
     <w lemma="than" pos="cs" reg="than" xml:id="A78888-001-a-1940">then</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-1950">the</w>
     <w lemma="parliament" pos="n1" xml:id="A78888-001-a-1960">Parliament</w>
     <w lemma="itself" pos="pr" reg="itself" xml:id="A78888-001-a-1970">it self</w>
     <pc xml:id="A78888-001-a-1990">,</pc>
     <w lemma="have" pos="vvz" xml:id="A78888-001-a-2000">Hath</w>
     <w lemma="think" pos="vvn" xml:id="A78888-001-a-2010">thought</w>
     <w lemma="fit" pos="j" xml:id="A78888-001-a-2020">fit</w>
     <w lemma="to" pos="prt" xml:id="A78888-001-a-2030">to</w>
     <w lemma="add" pos="vvi" reg="add" xml:id="A78888-001-a-2040">adde</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-2050">to</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-2060">His</w>
     <w lemma="last" pos="ord" xml:id="A78888-001-a-2070">last</w>
     <w lemma="message" pos="n1" xml:id="A78888-001-a-2080">Message</w>
     <pc xml:id="A78888-001-a-2090">,</pc>
     <w lemma="this" pos="d" xml:id="A78888-001-a-2100">this</w>
     <w lemma="profession" pos="n1" xml:id="A78888-001-a-2110">Profession</w>
     <pc xml:id="A78888-001-a-2120">,</pc>
     <w lemma="that" pos="cs" xml:id="A78888-001-a-2130">That</w>
     <w lemma="in" pos="acp" xml:id="A78888-001-a-2140">in</w>
     <w lemma="all" pos="d" xml:id="A78888-001-a-2150">all</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-2160">His</w>
     <w lemma="proceed" pos="n2-vg" xml:id="A78888-001-a-2170">Proceedings</w>
     <w lemma="against" pos="acp" xml:id="A78888-001-a-2180">against</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-2190">the</w>
     <w lemma="lord" pos="n1" xml:id="A78888-001-a-2200">Lord</w>
     <hi xml:id="A78888-e160">
      <w lemma="Kymbolton" pos="nn1" xml:id="A78888-001-a-2210">Kymbolton</w>
      <pc xml:id="A78888-001-a-2220">,</pc>
      <w lemma="mr" orig="Mʳ" pos="ab" xml:id="A78888-001-a-2230">Mr</w>
      <w lemma="hollis" pos="nn1" xml:id="A78888-001-a-2250">Hollis</w>
      <pc xml:id="A78888-001-a-2260">,</pc>
      <w lemma="sir" pos="n1" xml:id="A78888-001-a-2270">Sir</w>
      <w lemma="Arthur" pos="nn1" xml:id="A78888-001-a-2280">Arthur</w>
      <w lemma="Haslerig" pos="nn1" xml:id="A78888-001-a-2290">Haslerig</w>
      <pc xml:id="A78888-001-a-2300">,</pc>
      <w lemma="mr" orig="Mʳ" pos="ab" xml:id="A78888-001-a-2310">Mr</w>
      <w lemma="pym" pos="nn1" xml:id="A78888-001-a-2330">pym</w>
      <pc xml:id="A78888-001-a-2340">,</pc>
      <w lemma="mr" orig="Mʳ" pos="ab" xml:id="A78888-001-a-2350">Mr</w>
      <w lemma="hampden" pos="nn1" xml:id="A78888-001-a-2370">Hampden</w>
      <pc xml:id="A78888-001-a-2380">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-2390">and</w>
     <hi xml:id="A78888-e200">
      <w lemma="mr" orig="Mʳ" pos="ab" xml:id="A78888-001-a-2400">Mr</w>
      <w lemma="Strode" pos="nn1" xml:id="A78888-001-a-2420">Strode</w>
      <pc xml:id="A78888-001-a-2430">,</pc>
     </hi>
     <w lemma="he" pos="pns" xml:id="A78888-001-a-2440">He</w>
     <w lemma="have" pos="vvd" xml:id="A78888-001-a-2450">had</w>
     <w lemma="never" pos="avx" xml:id="A78888-001-a-2460">never</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-2470">the</w>
     <w lemma="least" pos="ds" xml:id="A78888-001-a-2480">least</w>
     <w lemma="intention" pos="n1" xml:id="A78888-001-a-2490">Intention</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-2500">of</w>
     <w lemma="violate" pos="vvg" xml:id="A78888-001-a-2510">violating</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-2520">the</w>
     <w lemma="least" pos="ds" xml:id="A78888-001-a-2530">least</w>
     <w lemma="privilege" pos="n1" reg="privilege" xml:id="A78888-001-a-2540">Priviledge</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-2550">of</w>
     <w lemma="parliament" pos="n1" xml:id="A78888-001-a-2560">Parliament</w>
     <pc xml:id="A78888-001-a-2570">;</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-2580">And</w>
     <w lemma="in" pos="acp" xml:id="A78888-001-a-2590">in</w>
     <w lemma="case" pos="n1" xml:id="A78888-001-a-2600">case</w>
     <w lemma="any" pos="d" xml:id="A78888-001-a-2610">any</w>
     <w lemma="doubt" pos="n1" xml:id="A78888-001-a-2620">doubt</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-2630">of</w>
     <w lemma="breach" pos="n1" xml:id="A78888-001-a-2640">breach</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-2650">of</w>
     <w lemma="privilege" pos="n2" reg="privileges" xml:id="A78888-001-a-2660">Priviledges</w>
     <w lemma="remain" pos="vvi" xml:id="A78888-001-a-2670">remain</w>
     <pc xml:id="A78888-001-a-2680">,</pc>
     <w lemma="will" pos="vmb" xml:id="A78888-001-a-2690">will</w>
     <w lemma="be" pos="vvi" xml:id="A78888-001-a-2700">be</w>
     <w lemma="willing" pos="j" xml:id="A78888-001-a-2710">willing</w>
     <w lemma="to" pos="prt" xml:id="A78888-001-a-2720">to</w>
     <w lemma="clear" pos="vvi" xml:id="A78888-001-a-2730">clear</w>
     <w lemma="that" pos="cs" xml:id="A78888-001-a-2740">that</w>
     <pc xml:id="A78888-001-a-2750">,</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-2760">and</w>
     <w lemma="assert" pos="vvi" xml:id="A78888-001-a-2770">assert</w>
     <w lemma="those" pos="d" xml:id="A78888-001-a-2780">those</w>
     <pc xml:id="A78888-001-a-2790">,</pc>
     <w lemma="by" pos="acp" xml:id="A78888-001-a-2800">by</w>
     <w lemma="any" pos="d" xml:id="A78888-001-a-2810">any</w>
     <w lemma="reasonable" pos="j" xml:id="A78888-001-a-2820">reasonable</w>
     <w lemma="way" pos="n1" xml:id="A78888-001-a-2830">way</w>
     <w lemma="that" pos="cs" xml:id="A78888-001-a-2840">that</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-2850">His</w>
     <w lemma="parliament" pos="n1" xml:id="A78888-001-a-2860">Parliament</w>
     <w lemma="shall" pos="vmb" xml:id="A78888-001-a-2870">shall</w>
     <w lemma="advise" pos="vvi" xml:id="A78888-001-a-2880">advise</w>
     <w lemma="he" pos="pno" xml:id="A78888-001-a-2890">Him</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-2900">to</w>
     <pc unit="sentence" xml:id="A78888-001-a-2910">.</pc>
     <w lemma="upon" pos="acp" reg="Upon" xml:id="A78888-001-a-2920">Vpon</w>
     <w lemma="confidence" pos="n1" xml:id="A78888-001-a-2930">confidence</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-2940">of</w>
     <w lemma="which" pos="crq" xml:id="A78888-001-a-2950">which</w>
     <pc xml:id="A78888-001-a-2960">,</pc>
     <w lemma="he" pos="pns" xml:id="A78888-001-a-2970">He</w>
     <w lemma="no" pos="dx" xml:id="A78888-001-a-2980">no</w>
     <w lemma="way" pos="n1" xml:id="A78888-001-a-2990">way</w>
     <w lemma="doubt" pos="vvz" xml:id="A78888-001-a-3000">doubts</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-3010">His</w>
     <w lemma="parliament" pos="n1" xml:id="A78888-001-a-3020">Parliament</w>
     <w lemma="will" pos="vmb" xml:id="A78888-001-a-3030">will</w>
     <w lemma="forthwith" pos="av" xml:id="A78888-001-a-3040">forthwith</w>
     <w lemma="lay" pos="vvi" xml:id="A78888-001-a-3050">lay</w>
     <w lemma="by" pos="acp" xml:id="A78888-001-a-3060">by</w>
     <w lemma="all" pos="d" xml:id="A78888-001-a-3070">all</w>
     <w lemma="jealousy" pos="n2" reg="jealousies" xml:id="A78888-001-a-3080">Iealousies</w>
     <pc xml:id="A78888-001-a-3090">,</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-3100">and</w>
     <w lemma="apply" pos="vvi" xml:id="A78888-001-a-3110">apply</w>
     <w lemma="themselves" pos="pr" xml:id="A78888-001-a-3120">themselves</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-3130">to</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-3140">the</w>
     <w lemma="public" pos="j" reg="public" xml:id="A78888-001-a-3150">Publike</w>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-3160">and</w>
     <w lemma="press" pos="j-vg" xml:id="A78888-001-a-3170">pressing</w>
     <w lemma="affair" pos="n2" xml:id="A78888-001-a-3180">Affairs</w>
     <pc xml:id="A78888-001-a-3190">,</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-3200">and</w>
     <w lemma="especial" pos="av-j" xml:id="A78888-001-a-3210">especially</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-3220">to</w>
     <w lemma="those" pos="d" xml:id="A78888-001-a-3230">those</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-3240">of</w>
     <hi xml:id="A78888-e220">
      <w lemma="Ireland" pos="nn1" xml:id="A78888-001-a-3250">Ireland</w>
      <pc xml:id="A78888-001-a-3260">,</pc>
     </hi>
     <w lemma="wherein" pos="crq" xml:id="A78888-001-a-3270">wherein</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-3280">the</w>
     <w lemma="good" pos="j" xml:id="A78888-001-a-3290">good</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-3300">of</w>
     <w lemma="this" pos="d" xml:id="A78888-001-a-3310">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A78888-001-a-3320">Kingdom</w>
     <pc xml:id="A78888-001-a-3330">,</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-3340">and</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-3350">the</w>
     <w lemma="true" pos="j" xml:id="A78888-001-a-3360">true</w>
     <w lemma="religion" pos="n1" xml:id="A78888-001-a-3370">Religion</w>
     <pc join="right" xml:id="A78888-001-a-3380">(</pc>
     <w lemma="which" pos="crq" xml:id="A78888-001-a-3390">which</w>
     <w lemma="shall" pos="vmb" xml:id="A78888-001-a-3400">shall</w>
     <w lemma="ever" pos="av" xml:id="A78888-001-a-3410">ever</w>
     <w lemma="be" pos="vvi" xml:id="A78888-001-a-3420">be</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-3430">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A78888-001-a-3440">Majesties</w>
     <w lemma="first" pos="ord" xml:id="A78888-001-a-3450">first</w>
     <w lemma="care" pos="n1" xml:id="A78888-001-a-3460">care</w>
     <pc xml:id="A78888-001-a-3470">)</pc>
     <w lemma="be" pos="vvb" xml:id="A78888-001-a-3480">are</w>
     <w lemma="so" pos="av" xml:id="A78888-001-a-3490">so</w>
     <w lemma="high" pos="av-j" xml:id="A78888-001-a-3500">highly</w>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-3510">and</w>
     <w lemma="so" pos="av" xml:id="A78888-001-a-3520">so</w>
     <w lemma="near" pos="av-j" reg="nearly" xml:id="A78888-001-a-3530">neerly</w>
     <w lemma="concern" pos="vvn" xml:id="A78888-001-a-3540">concerned</w>
     <pc xml:id="A78888-001-a-3550">:</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-3560">And</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-3570">His</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78888-001-a-3580">Majestie</w>
     <w lemma="assure" pos="vvz" xml:id="A78888-001-a-3590">assures</w>
     <w lemma="himself" pos="pr" xml:id="A78888-001-a-3600">himself</w>
     <pc xml:id="A78888-001-a-3610">,</pc>
     <w lemma="that" pos="cs" xml:id="A78888-001-a-3620">that</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-3630">His</w>
     <w lemma="care" pos="n1" xml:id="A78888-001-a-3640">care</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-3650">of</w>
     <w lemma="their" pos="po" xml:id="A78888-001-a-3660">their</w>
     <w lemma="privilege" pos="n2" reg="privileges" xml:id="A78888-001-a-3670">priviledges</w>
     <w lemma="will" pos="vmb" xml:id="A78888-001-a-3680">will</w>
     <w lemma="increase" pos="vvi" xml:id="A78888-001-a-3690">increase</w>
     <w lemma="their" pos="po" xml:id="A78888-001-a-3700">their</w>
     <w lemma="tenderness" pos="n1" reg="tenderness" xml:id="A78888-001-a-3710">tendernesse</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-3720">of</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-3730">His</w>
     <w lemma="lawful" pos="j" reg="lawful" xml:id="A78888-001-a-3740">lawfull</w>
     <w lemma="prerogative" pos="n1" xml:id="A78888-001-a-3750">Prerogative</w>
     <pc xml:id="A78888-001-a-3760">,</pc>
     <w lemma="which" pos="crq" xml:id="A78888-001-a-3770">which</w>
     <w lemma="be" pos="vvb" xml:id="A78888-001-a-3780">are</w>
     <w lemma="so" pos="av" xml:id="A78888-001-a-3790">so</w>
     <w lemma="necessary" pos="j" xml:id="A78888-001-a-3800">necessary</w>
     <w lemma="to" pos="acp" xml:id="A78888-001-a-3810">to</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-3820">the</w>
     <w lemma="mutual" pos="j" reg="mutual" xml:id="A78888-001-a-3830">mutuall</w>
     <w lemma="defence" pos="n1" xml:id="A78888-001-a-3840">defence</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-3850">of</w>
     <w lemma="each" pos="d" xml:id="A78888-001-a-3860">each</w>
     <w lemma="other" pos="pi-d" xml:id="A78888-001-a-3870">other</w>
     <pc xml:id="A78888-001-a-3880">;</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-3890">and</w>
     <w lemma="both" pos="d" xml:id="A78888-001-a-3900">both</w>
     <w lemma="which" pos="crq" xml:id="A78888-001-a-3910">which</w>
     <w lemma="will" pos="vmb" xml:id="A78888-001-a-3920">will</w>
     <w lemma="be" pos="vvi" xml:id="A78888-001-a-3930">be</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-3940">the</w>
     <w lemma="foundation" pos="n1" xml:id="A78888-001-a-3950">foundation</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-3960">of</w>
     <w lemma="a" pos="d" xml:id="A78888-001-a-3970">a</w>
     <w lemma="perpetual" pos="j" reg="perpetual" xml:id="A78888-001-a-3980">perpetuall</w>
     <w lemma="perfect" pos="j" xml:id="A78888-001-a-3990">perfect</w>
     <w lemma="intelligence" pos="n1" xml:id="A78888-001-a-4000">Intelligence</w>
     <w lemma="between" pos="acp" xml:id="A78888-001-a-4010">between</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-4020">His</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78888-001-a-4030">Majestie</w>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-4040">and</w>
     <w lemma="parliament" pos="n2" xml:id="A78888-001-a-4050">Parliaments</w>
     <pc xml:id="A78888-001-a-4060">,</pc>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-4070">and</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-4080">of</w>
     <w lemma="the" pos="d" xml:id="A78888-001-a-4090">the</w>
     <w lemma="happiness" pos="n1" reg="happiness" xml:id="A78888-001-a-4100">Happinesse</w>
     <w lemma="and" pos="cc" xml:id="A78888-001-a-4110">and</w>
     <w lemma="prosperity" pos="n1" xml:id="A78888-001-a-4120">Prosperity</w>
     <w lemma="of" pos="acp" xml:id="A78888-001-a-4130">of</w>
     <w lemma="his" pos="po" xml:id="A78888-001-a-4140">His</w>
     <w lemma="people" pos="n1" xml:id="A78888-001-a-4150">People</w>
     <pc unit="sentence" xml:id="A78888-001-a-4160">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A78888-e230">
   <div type="colophon" xml:id="A78888-e240">
    <p xml:id="A78888-e250">
     <pc xml:id="A78888-001-a-4170">¶</pc>
     <hi xml:id="A78888-e260">
      <w lemma="imprint" pos="vvn" xml:id="A78888-001-a-4180">Imprinted</w>
      <w lemma="at" pos="acp" xml:id="A78888-001-a-4190">at</w>
      <w lemma="London" pos="nn1" xml:id="A78888-001-a-4200">London</w>
      <w lemma="by" pos="acp" xml:id="A78888-001-a-4210">by</w>
      <w lemma="Robert" pos="nn1" xml:id="A78888-001-a-4220">Robert</w>
      <w lemma="Barker" pos="nn1" xml:id="A78888-001-a-4230">Barker</w>
      <pc xml:id="A78888-001-a-4240">,</pc>
      <w lemma="printer" pos="n1" xml:id="A78888-001-a-4250">Printer</w>
      <w lemma="to" pos="acp" xml:id="A78888-001-a-4260">to</w>
      <w lemma="the" pos="d" xml:id="A78888-001-a-4270">the</w>
      <w lemma="king" pos="n2" xml:id="A78888-001-a-4280">Kings</w>
      <w lemma="most" pos="avs-d" xml:id="A78888-001-a-4290">most</w>
      <w lemma="excellent" pos="j" xml:id="A78888-001-a-4300">Excellent</w>
      <w lemma="majesty" pos="n1" xml:id="A78888-001-a-4310">Majesty</w>
      <pc xml:id="A78888-001-a-4320">:</pc>
      <w lemma="and" pos="cc" xml:id="A78888-001-a-4330">And</w>
      <w lemma="by" pos="acp" xml:id="A78888-001-a-4340">by</w>
      <w lemma="the" pos="d" xml:id="A78888-001-a-4350">the</w>
      <w lemma="assign" pos="vvz" reg="assigns" xml:id="A78888-001-a-4360">Assignes</w>
      <w lemma="of" pos="acp" xml:id="A78888-001-a-4370">of</w>
      <w lemma="JOHN" pos="nn1" xml:id="A78888-001-a-4380">JOHN</w>
      <w lemma="bill" pos="nn1" xml:id="A78888-001-a-4390">BILL</w>
      <pc unit="sentence" xml:id="A78888-001-a-4400">.</pc>
      <w lemma="1641." pos="crd" xml:id="A78888-001-a-4410">1641.</w>
      <pc unit="sentence" xml:id="A78888-001-a-4420"/>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
