<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A78634">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majesties answer to a message sent to him by the House of Commons, concerning licences granted by him to persons to go into Ireland.</title>
    <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A78634 of text R209824 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.3[59]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A78634</idno>
    <idno type="STC">Wing C2100</idno>
    <idno type="STC">Thomason 669.f.3[59]</idno>
    <idno type="STC">ESTC R209824</idno>
    <idno type="EEBO-CITATION">99868680</idno>
    <idno type="PROQUEST">99868680</idno>
    <idno type="VID">160617</idno>
    <idno type="PROQUESTGOID">2240961313</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A78634)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160617)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f3[59])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majesties answer to a message sent to him by the House of Commons, concerning licences granted by him to persons to go into Ireland.</title>
      <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
      <author>Charles I, King of England, 1600-1649.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by Robert Barker, printer to the Kings most Excellent Majesty: and by the assignes of John Bill,</publisher>
      <pubPlace>Imprinted at London :</pubPlace>
      <date>1641 [i.e. 1642]</date>
     </publicationStmt>
     <notesStmt>
      <note>With engraving of royal seal at head of document.</note>
      <note>In this edition the imprint has 2 lines; line 1 ends: Kings.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Charles I, 1625-1649 -- Early works to 1800.</term>
     <term>Ireland -- History -- Rebellion of 1641 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A78634</ep:tcp>
    <ep:estc> R209824</ep:estc>
    <ep:stc> (Thomason 669.f.3[59]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>His Majesties answer to a message sent to him by the House of Commons, concerning licences granted by him to persons to go into Ireland.</ep:title>
    <ep:author>England and Wales. Sovereign</ep:author>
    <ep:publicationYear>1642</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>353</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-08</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-09</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-09</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A78634-e10">
  <body xml:id="A78634-e20">
   <pb facs="tcp:160617:1" rend="simple:additions" xml:id="A78634-001-a"/>
   <div type="text" xml:id="A78634-e30">
    <head xml:id="A78634-e40">
     <figure xml:id="A78634-e50">
      <figDesc xml:id="A78634-e60">Tudor rose</figDesc>
     </figure>
     <figure xml:id="A78634-e70">
      <p xml:id="A78634-e80">
       <w lemma="c" pos="sy" xml:id="A78634-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="A78634-001-a-0020">R</w>
      </p>
      <p xml:id="A78634-e90">
       <w lemma="dieu" pos="ffr" reg="Dieu" xml:id="A78634-001-a-0030">DIEV</w>
       <w lemma="et" pos="ffr" xml:id="A78634-001-a-0040">ET</w>
       <w lemma="mon" pos="ffr" xml:id="A78634-001-a-0050">MON</w>
       <w lemma="droit" pos="ffr" xml:id="A78634-001-a-0060">DROIT</w>
      </p>
      <p xml:id="A78634-e100">
       <w lemma="honi" pos="ffr" xml:id="A78634-001-a-0070">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A78634-001-a-0080">SOIT</w>
       <w lemma="qvi" pos="ffr" reg="x" xml:id="A78634-001-a-0090">QVI</w>
       <w lemma="mal" pos="ffr" xml:id="A78634-001-a-0100">MAL</w>
       <w lemma="y" pos="ffr" xml:id="A78634-001-a-0110">Y</w>
       <w lemma="PENSE" pos="ffr" xml:id="A78634-001-a-0120">PENSE</w>
      </p>
      <figDesc xml:id="A78634-e110">royal blazon or coat of arms</figDesc>
     </figure>
     <figure xml:id="A78634-e120">
      <figDesc xml:id="A78634-e130">Scottish thistle</figDesc>
     </figure>
    </head>
    <head xml:id="A78634-e140">
     <w lemma="❧" pos="sy" xml:id="A78634-001-a-0130">❧</w>
     <w lemma="his" pos="po" xml:id="A78634-001-a-0140">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A78634-001-a-0150">Majesties</w>
     <w lemma="answer" pos="n1" xml:id="A78634-001-a-0160">Answer</w>
     <w lemma="to" pos="acp" xml:id="A78634-001-a-0170">to</w>
     <w lemma="a" pos="d" xml:id="A78634-001-a-0180">a</w>
     <w lemma="message" pos="n1" xml:id="A78634-001-a-0190">Message</w>
     <w lemma="send" pos="vvn" xml:id="A78634-001-a-0200">sent</w>
     <w lemma="to" pos="acp" xml:id="A78634-001-a-0210">to</w>
     <w lemma="he" pos="pno" xml:id="A78634-001-a-0220">Him</w>
     <w lemma="by" pos="acp" xml:id="A78634-001-a-0230">by</w>
     <w lemma="the" pos="d" xml:id="A78634-001-a-0240">the</w>
     <w lemma="house" pos="n1" xml:id="A78634-001-a-0250">House</w>
     <w lemma="of" pos="acp" xml:id="A78634-001-a-0260">of</w>
     <w lemma="commons" pos="n2" xml:id="A78634-001-a-0270">Commons</w>
     <pc xml:id="A78634-001-a-0280">,</pc>
     <w lemma="concerning" pos="acp" xml:id="A78634-001-a-0290">concerning</w>
     <w lemma="licence" pos="n2" xml:id="A78634-001-a-0300">Licences</w>
     <w lemma="grant" pos="vvn" xml:id="A78634-001-a-0310">granted</w>
     <w lemma="by" pos="acp" xml:id="A78634-001-a-0320">by</w>
     <w lemma="he" pos="pno" xml:id="A78634-001-a-0330">Him</w>
     <w lemma="to" pos="acp" xml:id="A78634-001-a-0340">to</w>
     <w lemma="person" pos="n2" xml:id="A78634-001-a-0350">persons</w>
     <w lemma="to" pos="prt" xml:id="A78634-001-a-0360">to</w>
     <w lemma="go" pos="vvi" xml:id="A78634-001-a-0370">go</w>
     <w lemma="into" pos="acp" xml:id="A78634-001-a-0380">into</w>
     <hi xml:id="A78634-e150">
      <w lemma="Ireland" pos="nn1" xml:id="A78634-001-a-0390">Ireland</w>
      <pc unit="sentence" xml:id="A78634-001-a-0400">.</pc>
     </hi>
    </head>
    <p xml:id="A78634-e160">
     <w lemma="his" pos="po" rend="decorinit" xml:id="A78634-001-a-0410">HIs</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78634-001-a-0420">Majestie</w>
     <w lemma="have" pos="vvz" xml:id="A78634-001-a-0430">hath</w>
     <w lemma="see" pos="vvn" xml:id="A78634-001-a-0440">seen</w>
     <w lemma="and" pos="cc" xml:id="A78634-001-a-0450">and</w>
     <w lemma="consider" pos="vvn" xml:id="A78634-001-a-0460">considered</w>
     <w lemma="the" pos="d" xml:id="A78634-001-a-0470">the</w>
     <w lemma="message" pos="n1" xml:id="A78634-001-a-0480">Message</w>
     <w lemma="present" pos="vvn" xml:id="A78634-001-a-0490">presented</w>
     <w lemma="to" pos="acp" xml:id="A78634-001-a-0500">to</w>
     <w lemma="he" pos="pno" xml:id="A78634-001-a-0510">Him</w>
     <w lemma="by" pos="acp" xml:id="A78634-001-a-0520">by</w>
     <w lemma="the" pos="d" xml:id="A78634-001-a-0530">the</w>
     <w lemma="lord" pos="n1" xml:id="A78634-001-a-0540">Lord</w>
     <hi xml:id="A78634-e170">
      <w lemma="Compton" pos="nn1" xml:id="A78634-001-a-0550">Compton</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A78634-001-a-0560">and</w>
     <w lemma="master" pos="n1" xml:id="A78634-001-a-0570">Master</w>
     <hi xml:id="A78634-e180">
      <w lemma="baynton" pos="nn1" xml:id="A78634-001-a-0580">Baynton</w>
      <pc xml:id="A78634-001-a-0590">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A78634-001-a-0600">the</w>
     <w lemma="19" orig="19ᵗʰ" pos="ord" xml:id="A78634-001-a-0610">19th</w>
     <w lemma="of" pos="acp" xml:id="A78634-001-a-0630">of</w>
     <w lemma="march" pos="nn1" xml:id="A78634-001-a-0640">March</w>
     <pc xml:id="A78634-001-a-0650">,</pc>
     <w lemma="1641." pos="crd" xml:id="A78634-001-a-0660">1641.</w>
     <w lemma="at" pos="acp" xml:id="A78634-001-a-0670">at</w>
     <hi xml:id="A78634-e200">
      <w lemma="York" pos="nn1" xml:id="A78634-001-a-0680">York</w>
      <pc xml:id="A78634-001-a-0690">,</pc>
     </hi>
     <w lemma="touch" pos="vvg" xml:id="A78634-001-a-0700">Touching</w>
     <w lemma="such" pos="d" xml:id="A78634-001-a-0710">such</w>
     <w lemma="person" pos="n2" xml:id="A78634-001-a-0720">persons</w>
     <w lemma="as" pos="acp" xml:id="A78634-001-a-0730">as</w>
     <w lemma="have" pos="vvi" xml:id="A78634-001-a-0740">have</w>
     <w lemma="be" pos="vvn" xml:id="A78634-001-a-0750">been</w>
     <w lemma="license" pos="vvn" reg="licenced" xml:id="A78634-001-a-0760">Licensed</w>
     <w lemma="by" pos="acp" xml:id="A78634-001-a-0770">by</w>
     <w lemma="his" pos="po" xml:id="A78634-001-a-0780">his</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78634-001-a-0790">Majestie</w>
     <w lemma="to" pos="prt" xml:id="A78634-001-a-0800">to</w>
     <w lemma="pass" pos="vvi" reg="pass" xml:id="A78634-001-a-0810">passe</w>
     <w lemma="into" pos="acp" xml:id="A78634-001-a-0820">into</w>
     <hi xml:id="A78634-e210">
      <w lemma="Ireland" pos="nn1" xml:id="A78634-001-a-0830">Ireland</w>
      <pc unit="sentence" xml:id="A78634-001-a-0840">.</pc>
     </hi>
    </p>
    <p xml:id="A78634-e220">
     <w lemma="though" pos="cs" xml:id="A78634-001-a-0850">Though</w>
     <w lemma="he" pos="pns" xml:id="A78634-001-a-0860">He</w>
     <w lemma="will" pos="vmb" xml:id="A78634-001-a-0870">will</w>
     <w lemma="not" pos="xx" xml:id="A78634-001-a-0880">not</w>
     <w lemma="insist" pos="vvi" xml:id="A78634-001-a-0890">insist</w>
     <w lemma="upon" pos="acp" xml:id="A78634-001-a-0900">upon</w>
     <w lemma="what" pos="crq" xml:id="A78634-001-a-0910">what</w>
     <w lemma="little" pos="j" xml:id="A78634-001-a-0920">little</w>
     <w lemma="reason" pos="n1" xml:id="A78634-001-a-0930">Reason</w>
     <w lemma="they" pos="pns" xml:id="A78634-001-a-0940">they</w>
     <w lemma="have" pos="vvd" xml:id="A78634-001-a-0950">had</w>
     <w lemma="to" pos="prt" xml:id="A78634-001-a-0960">to</w>
     <w lemma="suspect" pos="vvi" xml:id="A78634-001-a-0970">suspect</w>
     <w lemma="that" pos="cs" xml:id="A78634-001-a-0980">that</w>
     <w lemma="some" pos="d" xml:id="A78634-001-a-0990">some</w>
     <w lemma="ill-affected" pos="j" xml:id="A78634-001-a-1000">ill-affected</w>
     <w lemma="person" pos="n2" xml:id="A78634-001-a-1010">persons</w>
     <w lemma="have" pos="vvd" xml:id="A78634-001-a-1020">had</w>
     <w lemma="pass" pos="vvn" xml:id="A78634-001-a-1030">passed</w>
     <w lemma="into" pos="acp" xml:id="A78634-001-a-1040">into</w>
     <hi xml:id="A78634-e230">
      <w lemma="Ireland" pos="nn1" xml:id="A78634-001-a-1050">Ireland</w>
      <pc xml:id="A78634-001-a-1060">,</pc>
     </hi>
     <w lemma="under" pos="acp" xml:id="A78634-001-a-1070">under</w>
     <w lemma="colour" pos="n1" xml:id="A78634-001-a-1080">colour</w>
     <w lemma="of" pos="acp" xml:id="A78634-001-a-1090">of</w>
     <w lemma="his" pos="po" xml:id="A78634-001-a-1100">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A78634-001-a-1110">Majesties</w>
     <w lemma="licence" pos="n1" xml:id="A78634-001-a-1120">Licence</w>
     <pc join="right" xml:id="A78634-001-a-1130">(</pc>
     <w lemma="inference" pos="n2" xml:id="A78634-001-a-1140">Inferences</w>
     <w lemma="be" pos="vvg" xml:id="A78634-001-a-1150">being</w>
     <w lemma="slender" pos="j" xml:id="A78634-001-a-1160">slender</w>
     <w lemma="proof" pos="n2" xml:id="A78634-001-a-1170">Proofs</w>
     <w lemma="to" pos="acp" xml:id="A78634-001-a-1180">to</w>
     <w lemma="ground" pos="n1" xml:id="A78634-001-a-1190">ground</w>
     <w lemma="belief" pos="n1" xml:id="A78634-001-a-1200">belief</w>
     <w lemma="upon" pos="acp" xml:id="A78634-001-a-1210">upon</w>
     <pc xml:id="A78634-001-a-1220">)</pc>
     <w lemma="yet" pos="av" xml:id="A78634-001-a-1230">yet</w>
     <w lemma="he" pos="pns" xml:id="A78634-001-a-1240">He</w>
     <w lemma="must" pos="vmb" xml:id="A78634-001-a-1250">must</w>
     <w lemma="needs" pos="av" xml:id="A78634-001-a-1260">needs</w>
     <w lemma="avow" pos="vvi" xml:id="A78634-001-a-1270">avow</w>
     <pc xml:id="A78634-001-a-1280">,</pc>
     <w lemma="that" pos="cs" xml:id="A78634-001-a-1290">That</w>
     <w lemma="for" pos="acp" xml:id="A78634-001-a-1300">for</w>
     <w lemma="any" pos="d" xml:id="A78634-001-a-1310">any</w>
     <w lemma="thing" pos="n1" xml:id="A78634-001-a-1320">thing</w>
     <w lemma="that" pos="cs" xml:id="A78634-001-a-1330">that</w>
     <w lemma="be" pos="vvz" xml:id="A78634-001-a-1340">is</w>
     <w lemma="yet" pos="av" xml:id="A78634-001-a-1350">yet</w>
     <w lemma="declare" pos="vvn" xml:id="A78634-001-a-1360">Declared</w>
     <pc xml:id="A78634-001-a-1370">,</pc>
     <w lemma="he" pos="pns" xml:id="A78634-001-a-1380">He</w>
     <w lemma="can" pos="vmbx" xml:id="A78634-001-a-1390">cannot</w>
     <w lemma="see" pos="vvi" xml:id="A78634-001-a-1400">see</w>
     <w lemma="any" pos="d" xml:id="A78634-001-a-1410">any</w>
     <w lemma="ground" pos="n1" xml:id="A78634-001-a-1420">ground</w>
     <w lemma="why" pos="crq" xml:id="A78634-001-a-1430">why</w>
     <w lemma="master" pos="n1" xml:id="A78634-001-a-1440">Master</w>
     <hi xml:id="A78634-e240">
      <w lemma="Pym" pos="nn1" xml:id="A78634-001-a-1450">Pym</w>
     </hi>
     <w lemma="shall" pos="vmd" xml:id="A78634-001-a-1460">should</w>
     <w lemma="so" pos="av" xml:id="A78634-001-a-1470">so</w>
     <w lemma="bold" pos="av-j" reg="boldly" xml:id="A78634-001-a-1480">bouldly</w>
     <w lemma="affirm" pos="vvi" xml:id="A78634-001-a-1490">affirm</w>
     <w lemma="before" pos="acp" xml:id="A78634-001-a-1500">before</w>
     <w lemma="both" pos="d" xml:id="A78634-001-a-1510">both</w>
     <w lemma="house" pos="n2" xml:id="A78634-001-a-1520">Houses</w>
     <w lemma="of" pos="acp" xml:id="A78634-001-a-1530">of</w>
     <w lemma="parliament" pos="n1" xml:id="A78634-001-a-1540">Parliament</w>
     <pc xml:id="A78634-001-a-1550">,</pc>
     <hi xml:id="A78634-e250">
      <w lemma="that" pos="cs" xml:id="A78634-001-a-1560">That</w>
      <w lemma="since" pos="acp" xml:id="A78634-001-a-1570">since</w>
      <w lemma="the" pos="d" xml:id="A78634-001-a-1580">the</w>
      <w lemma="stop" pos="n1" xml:id="A78634-001-a-1590">stop</w>
      <w lemma="upon" pos="acp" xml:id="A78634-001-a-1600">upon</w>
      <w lemma="the" pos="d" xml:id="A78634-001-a-1610">the</w>
      <w lemma="port" pos="n2" xml:id="A78634-001-a-1620">Ports</w>
      <w lemma="by" pos="acp" xml:id="A78634-001-a-1630">by</w>
      <w lemma="both" pos="d" xml:id="A78634-001-a-1640">both</w>
      <w lemma="house" pos="n2" xml:id="A78634-001-a-1650">Houses</w>
      <w lemma="against" pos="acp" xml:id="A78634-001-a-1660">against</w>
      <w lemma="all" pos="d" xml:id="A78634-001-a-1670">all</w>
      <w lemma="irish" pos="jnn" xml:id="A78634-001-a-1680">Irish</w>
      <w lemma="papist" pos="nn2" xml:id="A78634-001-a-1690">Papists</w>
      <pc xml:id="A78634-001-a-1700">,</pc>
      <w lemma="many" pos="d" xml:id="A78634-001-a-1710">many</w>
      <w lemma="of" pos="acp" xml:id="A78634-001-a-1720">of</w>
      <w lemma="the" pos="d" xml:id="A78634-001-a-1730">the</w>
      <w lemma="chief" pos="j" xml:id="A78634-001-a-1740">chief</w>
      <w lemma="commander" pos="n2" xml:id="A78634-001-a-1750">Commanders</w>
      <w lemma="now" pos="av" xml:id="A78634-001-a-1760">now</w>
      <w lemma="in" pos="acp" xml:id="A78634-001-a-1770">in</w>
      <w lemma="the" pos="d" xml:id="A78634-001-a-1780">the</w>
      <w lemma="head" pos="n1" xml:id="A78634-001-a-1790">head</w>
      <w lemma="of" pos="acp" xml:id="A78634-001-a-1800">of</w>
      <w lemma="the" pos="d" xml:id="A78634-001-a-1810">the</w>
      <w lemma="rebel" pos="n2" reg="rebels" xml:id="A78634-001-a-1820">Rebells</w>
      <pc xml:id="A78634-001-a-1830">,</pc>
      <w lemma="have" pos="vvb" xml:id="A78634-001-a-1840">have</w>
      <w lemma="be" pos="vvn" xml:id="A78634-001-a-1850">been</w>
      <w lemma="suffer" pos="vvn" xml:id="A78634-001-a-1860">suffered</w>
      <w lemma="to" pos="prt" xml:id="A78634-001-a-1870">to</w>
      <w lemma="pass" pos="vvi" reg="pass" xml:id="A78634-001-a-1880">passe</w>
      <w lemma="by" pos="acp" xml:id="A78634-001-a-1890">by</w>
      <w lemma="his" pos="po" xml:id="A78634-001-a-1900">his</w>
      <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A78634-001-a-1910">Majesties</w>
      <w lemma="immediate" pos="j" xml:id="A78634-001-a-1920">immediate</w>
      <w lemma="warrant" pos="n1" xml:id="A78634-001-a-1930">Warrant</w>
      <pc xml:id="A78634-001-a-1940">;</pc>
     </hi>
     <w lemma="for" pos="acp" xml:id="A78634-001-a-1950">For</w>
     <w lemma="as" pos="acp" xml:id="A78634-001-a-1960">as</w>
     <w lemma="yet" pos="av" xml:id="A78634-001-a-1970">yet</w>
     <w lemma="there" pos="av" xml:id="A78634-001-a-1980">there</w>
     <w lemma="be" pos="vvz" xml:id="A78634-001-a-1990">is</w>
     <w lemma="not" pos="xx" xml:id="A78634-001-a-2000">not</w>
     <w lemma="any" pos="d" xml:id="A78634-001-a-2010">any</w>
     <w lemma="particular" pos="j" xml:id="A78634-001-a-2020">particular</w>
     <w lemma="person" pos="n1" xml:id="A78634-001-a-2030">person</w>
     <w lemma="name" pos="vvn" xml:id="A78634-001-a-2040">named</w>
     <w lemma="that" pos="cs" xml:id="A78634-001-a-2050">that</w>
     <w lemma="be" pos="vvz" xml:id="A78634-001-a-2060">is</w>
     <w lemma="now" pos="av" xml:id="A78634-001-a-2070">now</w>
     <w lemma="so" pos="av" xml:id="A78634-001-a-2080">so</w>
     <w lemma="much" pos="av-d" xml:id="A78634-001-a-2090">much</w>
     <w lemma="as" pos="acp" xml:id="A78634-001-a-2100">as</w>
     <w lemma="in" pos="acp" xml:id="A78634-001-a-2110">in</w>
     <w lemma="rebellion" pos="n1" xml:id="A78634-001-a-2120">Rebellion</w>
     <pc join="right" xml:id="A78634-001-a-2130">(</pc>
     <w lemma="much" pos="av-d" xml:id="A78634-001-a-2140">much</w>
     <w lemma="less" pos="dc" reg="less" xml:id="A78634-001-a-2150">lesse</w>
     <w lemma="in" pos="acp" xml:id="A78634-001-a-2160">in</w>
     <w lemma="the" pos="d" xml:id="A78634-001-a-2170">the</w>
     <w lemma="head" pos="n1" xml:id="A78634-001-a-2180">head</w>
     <w lemma="of" pos="acp" xml:id="A78634-001-a-2190">of</w>
     <w lemma="the" pos="d" xml:id="A78634-001-a-2200">the</w>
     <w lemma="rebel" pos="n2" reg="rebels" xml:id="A78634-001-a-2210">Rebells</w>
     <pc xml:id="A78634-001-a-2220">)</pc>
     <w lemma="to" pos="acp" xml:id="A78634-001-a-2230">to</w>
     <w lemma="who" pos="crq" xml:id="A78634-001-a-2240">whom</w>
     <w lemma="his" pos="po" xml:id="A78634-001-a-2250">His</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78634-001-a-2260">Majestie</w>
     <w lemma="have" pos="vvz" xml:id="A78634-001-a-2270">hath</w>
     <w lemma="give" pos="vvn" xml:id="A78634-001-a-2280">given</w>
     <w lemma="licence" pos="n1" xml:id="A78634-001-a-2290">Licence</w>
     <pc xml:id="A78634-001-a-2300">;</pc>
    </p>
    <p xml:id="A78634-e260">
     <w lemma="and" pos="cc" xml:id="A78634-001-a-2310">And</w>
     <w lemma="therefore" pos="av" xml:id="A78634-001-a-2320">therefore</w>
     <w lemma="according" pos="j" xml:id="A78634-001-a-2330">according</w>
     <w lemma="to" pos="acp" xml:id="A78634-001-a-2340">to</w>
     <w lemma="his" pos="po" xml:id="A78634-001-a-2350">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A78634-001-a-2360">Majesties</w>
     <w lemma="reply" pos="n1" xml:id="A78634-001-a-2370">Reply</w>
     <w lemma="upon" pos="acp" xml:id="A78634-001-a-2380">upon</w>
     <w lemma="that" pos="d" xml:id="A78634-001-a-2390">that</w>
     <w lemma="subject" pos="j" xml:id="A78634-001-a-2400">Subject</w>
     <pc xml:id="A78634-001-a-2410">,</pc>
     <w lemma="his" pos="po" xml:id="A78634-001-a-2420">His</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78634-001-a-2430">Majestie</w>
     <w lemma="expect" pos="vvz" xml:id="A78634-001-a-2440">expects</w>
     <pc xml:id="A78634-001-a-2450">,</pc>
     <w lemma="that" pos="cs" xml:id="A78634-001-a-2460">That</w>
     <w lemma="his" pos="po" xml:id="A78634-001-a-2470">His</w>
     <w lemma="house" pos="n1" xml:id="A78634-001-a-2480">House</w>
     <w lemma="of" pos="acp" xml:id="A78634-001-a-2490">of</w>
     <w lemma="commons" pos="n2" xml:id="A78634-001-a-2500">Commons</w>
     <w lemma="publish" pos="vvi" xml:id="A78634-001-a-2510">publish</w>
     <w lemma="such" pos="d" xml:id="A78634-001-a-2520">such</w>
     <w lemma="a" pos="d" xml:id="A78634-001-a-2530">a</w>
     <w lemma="declaration" pos="n1" xml:id="A78634-001-a-2540">Declaration</w>
     <pc xml:id="A78634-001-a-2550">,</pc>
     <w lemma="whereby" pos="crq" xml:id="A78634-001-a-2560">whereby</w>
     <w lemma="this" pos="d" xml:id="A78634-001-a-2570">this</w>
     <w lemma="mistake" pos="vvg" xml:id="A78634-001-a-2580">mistaking</w>
     <w lemma="may" pos="vmb" xml:id="A78634-001-a-2590">may</w>
     <w lemma="be" pos="vvi" xml:id="A78634-001-a-2600">be</w>
     <w lemma="clear" pos="vvn" xml:id="A78634-001-a-2610">cleared</w>
     <pc xml:id="A78634-001-a-2620">,</pc>
     <w lemma="that" pos="cs" xml:id="A78634-001-a-2630">That</w>
     <w lemma="so" pos="av" xml:id="A78634-001-a-2640">so</w>
     <w lemma="all" pos="d" xml:id="A78634-001-a-2650">all</w>
     <w lemma="the" pos="d" xml:id="A78634-001-a-2660">the</w>
     <w lemma="world" pos="n1" xml:id="A78634-001-a-2670">World</w>
     <w lemma="may" pos="vmb" xml:id="A78634-001-a-2680">may</w>
     <w lemma="see" pos="vvi" xml:id="A78634-001-a-2690">see</w>
     <w lemma="his" pos="po" xml:id="A78634-001-a-2700">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A78634-001-a-2710">Majesties</w>
     <w lemma="caution" pos="n1" xml:id="A78634-001-a-2720">Caution</w>
     <w lemma="in" pos="acp" xml:id="A78634-001-a-2730">in</w>
     <w lemma="give" pos="vvg" xml:id="A78634-001-a-2740">giving</w>
     <w lemma="of" pos="acp" xml:id="A78634-001-a-2750">of</w>
     <w lemma="pass" pos="n2" xml:id="A78634-001-a-2760">Passes</w>
     <pc xml:id="A78634-001-a-2770">;</pc>
     <w lemma="and" pos="cc" xml:id="A78634-001-a-2780">and</w>
     <w lemma="likewise" pos="av" xml:id="A78634-001-a-2790">likewise</w>
     <pc xml:id="A78634-001-a-2800">,</pc>
     <w lemma="that" pos="cs" xml:id="A78634-001-a-2810">That</w>
     <w lemma="his" pos="po" xml:id="A78634-001-a-2820">His</w>
     <w lemma="minister" pos="n2" xml:id="A78634-001-a-2830">Ministers</w>
     <w lemma="have" pos="vvb" xml:id="A78634-001-a-2840">have</w>
     <w lemma="not" pos="xx" xml:id="A78634-001-a-2850">not</w>
     <w lemma="abuse" pos="vvn" xml:id="A78634-001-a-2860">abused</w>
     <w lemma="his" pos="po" xml:id="A78634-001-a-2870">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A78634-001-a-2880">Majesties</w>
     <w lemma="trust" pos="n1" xml:id="A78634-001-a-2890">Trust</w>
     <pc xml:id="A78634-001-a-2900">,</pc>
     <w lemma="by" pos="acp" xml:id="A78634-001-a-2910">by</w>
     <w lemma="any" pos="d" xml:id="A78634-001-a-2920">any</w>
     <w lemma="surreptitious" pos="j" xml:id="A78634-001-a-2930">surreptitious</w>
     <w lemma="warrant" pos="n2" xml:id="A78634-001-a-2940">Warrants</w>
     <pc unit="sentence" xml:id="A78634-001-a-2950">.</pc>
    </p>
    <p xml:id="A78634-e270">
     <w lemma="and" pos="cc" xml:id="A78634-001-a-2960">And</w>
     <w lemma="last" pos="ord" xml:id="A78634-001-a-2970">lastly</w>
     <pc xml:id="A78634-001-a-2980">,</pc>
     <w lemma="his" pos="po" xml:id="A78634-001-a-2990">His</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78634-001-a-3000">Majestie</w>
     <w lemma="expect" pos="vvz" xml:id="A78634-001-a-3010">expects</w>
     <pc xml:id="A78634-001-a-3020">,</pc>
     <w lemma="that" pos="cs" xml:id="A78634-001-a-3030">That</w>
     <w lemma="henceforth" pos="av" xml:id="A78634-001-a-3040">henceforth</w>
     <w lemma="there" pos="av" xml:id="A78634-001-a-3050">there</w>
     <w lemma="be" pos="vvi" xml:id="A78634-001-a-3060">be</w>
     <w lemma="more" pos="dc" xml:id="A78634-001-a-3070">more</w>
     <w lemma="wariness" pos="n1" reg="wariness" xml:id="A78634-001-a-3080">Warinesse</w>
     <w lemma="use" pos="vvn" xml:id="A78634-001-a-3090">used</w>
     <pc xml:id="A78634-001-a-3100">,</pc>
     <w lemma="before" pos="acp" xml:id="A78634-001-a-3110">before</w>
     <w lemma="such" pos="d" xml:id="A78634-001-a-3120">such</w>
     <w lemma="public" pos="j" reg="public" xml:id="A78634-001-a-3130">publike</w>
     <w lemma="aspersion" pos="n2" xml:id="A78634-001-a-3140">Aspersions</w>
     <w lemma="be" pos="vvb" xml:id="A78634-001-a-3150">be</w>
     <w lemma="lay" pos="vvn" xml:id="A78634-001-a-3160">laid</w>
     <pc xml:id="A78634-001-a-3170">,</pc>
     <w lemma="unless" pos="cs" reg="unless" xml:id="A78634-001-a-3180">unlesse</w>
     <w lemma="the" pos="d" xml:id="A78634-001-a-3190">the</w>
     <w lemma="ground" pos="n2" xml:id="A78634-001-a-3200">Grounds</w>
     <w lemma="be" pos="vvb" xml:id="A78634-001-a-3210">be</w>
     <w lemma="beforehand" pos="av" xml:id="A78634-001-a-3220">beforehand</w>
     <w lemma="better" pos="avc-j" xml:id="A78634-001-a-3230">better</w>
     <w lemma="warrant" pos="vvn" xml:id="A78634-001-a-3240">warranted</w>
     <w lemma="by" pos="acp" xml:id="A78634-001-a-3250">by</w>
     <w lemma="sufficient" pos="j" xml:id="A78634-001-a-3260">sufficient</w>
     <w lemma="proof" pos="n2" xml:id="A78634-001-a-3270">Proofs</w>
     <pc unit="sentence" xml:id="A78634-001-a-3280">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A78634-e280">
   <div type="colophon" xml:id="A78634-e290">
    <p xml:id="A78634-e300">
     <pc xml:id="A78634-001-a-3290">¶</pc>
     <w lemma="imprint" pos="vvn" xml:id="A78634-001-a-3300">Imprinted</w>
     <w lemma="at" pos="acp" xml:id="A78634-001-a-3310">at</w>
     <w lemma="London" pos="nn1" xml:id="A78634-001-a-3320">London</w>
     <w lemma="by" pos="acp" xml:id="A78634-001-a-3330">by</w>
     <w lemma="Robert" pos="nn1" xml:id="A78634-001-a-3340">Robert</w>
     <w lemma="Barker" pos="nn1" xml:id="A78634-001-a-3350">Barker</w>
     <pc xml:id="A78634-001-a-3360">,</pc>
     <w lemma="printer" pos="n1" xml:id="A78634-001-a-3370">Printer</w>
     <w lemma="to" pos="acp" xml:id="A78634-001-a-3380">to</w>
     <w lemma="the" pos="d" xml:id="A78634-001-a-3390">the</w>
     <w lemma="king" pos="n2" xml:id="A78634-001-a-3400">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A78634-001-a-3410">most</w>
     <w lemma="excellent" pos="j" xml:id="A78634-001-a-3420">Excellent</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78634-001-a-3430">Majestie</w>
     <pc xml:id="A78634-001-a-3440">:</pc>
     <w lemma="and" pos="cc" xml:id="A78634-001-a-3450">And</w>
     <w lemma="by" pos="acp" xml:id="A78634-001-a-3460">by</w>
     <w lemma="the" pos="d" xml:id="A78634-001-a-3470">the</w>
     <w lemma="assign" pos="vvz" reg="assigns" xml:id="A78634-001-a-3480">Assignes</w>
     <w lemma="of" pos="acp" xml:id="A78634-001-a-3490">of</w>
     <w lemma="John" pos="nn1" xml:id="A78634-001-a-3500">John</w>
     <w lemma="bill" pos="nn1" xml:id="A78634-001-a-3510">Bill</w>
     <pc unit="sentence" xml:id="A78634-001-a-3520">.</pc>
     <w lemma="1641." pos="crd" xml:id="A78634-001-a-3530">1641.</w>
     <pc unit="sentence" xml:id="A78634-001-a-3540"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
