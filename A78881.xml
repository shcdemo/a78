<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A78881">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majesties message sent by the Lord Chamberlain to the House of Peers, the 28. of December. 1641.</title>
    <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A78881 of text R209721 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.3[28]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A78881</idno>
    <idno type="STC">Wing C2435</idno>
    <idno type="STC">Thomason 669.f.3[28]</idno>
    <idno type="STC">ESTC R209721</idno>
    <idno type="EEBO-CITATION">99868588</idno>
    <idno type="PROQUEST">99868588</idno>
    <idno type="VID">160586</idno>
    <idno type="PROQUESTGOID">2248509308</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A78881)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160586)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f3[28])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majesties message sent by the Lord Chamberlain to the House of Peers, the 28. of December. 1641.</title>
      <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
      <author>Charles I, King of England, 1600-1649.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by Robert Barker, printer to the Kings most Excellent Majesty: and by the assignes of John Bill,</publisher>
      <pubPlace>Imprinted at London :</pubPlace>
      <date>1641.</date>
     </publicationStmt>
     <notesStmt>
      <note>With engraving of royal seal of Charles I at head of document.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Ireland -- History -- Rebellion of 1641 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A78881</ep:tcp>
    <ep:estc> R209721</ep:estc>
    <ep:stc> (Thomason 669.f.3[28]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>His Majesties message sent by the Lord Chamberlain to the House of Peers, the 28. of December. 1641.</ep:title>
    <ep:author>England and Wales. Sovereign</ep:author>
    <ep:publicationYear>1641</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>184</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-04</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-05</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change><date>2008-05</date><label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A78881-e10">
  <body xml:id="A78881-e20">
   <pb facs="tcp:160586:1" rend="simple:additions" xml:id="A78881-001-a"/>
   <div type="text" xml:id="A78881-e30">
    <head xml:id="A78881-e40">
     <figure xml:id="A78881-e50">
      <figDesc xml:id="A78881-e60">royal blazon or coat of arms</figDesc>
      <p xml:id="A78881-e70">
       <w lemma="c" pos="sy" xml:id="A78881-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="A78881-001-a-0020">R</w>
      </p>
      <p xml:id="A78881-e80">
       <w lemma="honi" pos="ffr" xml:id="A78881-001-a-0030">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A78881-001-a-0040">SOIT</w>
       <w lemma="qvi" pos="ffr" reg="x" xml:id="A78881-001-a-0050">QVI</w>
       <w lemma="mal" pos="ffr" xml:id="A78881-001-a-0060">MAL</w>
       <w lemma="y" pos="ffr" xml:id="A78881-001-a-0070">Y</w>
       <w lemma="PENSE" pos="ffr" xml:id="A78881-001-a-0080">PENSE</w>
      </p>
      <p xml:id="A78881-e90">
       <w lemma="dieu" pos="ffr" reg="Dieu" xml:id="A78881-001-a-0090">DIEV</w>
       <w lemma="et" pos="ffr" xml:id="A78881-001-a-0100">ET</w>
       <w lemma="mon" pos="ffr" xml:id="A78881-001-a-0110">MON</w>
       <w lemma="droit" pos="ffr" xml:id="A78881-001-a-0120">DROIT</w>
       <pc unit="sentence" xml:id="A78881-001-a-0130">.</pc>
      </p>
     </figure>
     <w lemma="❧" pos="sy" xml:id="A78881-001-a-0140">❧</w>
     <w lemma="his" pos="po" xml:id="A78881-001-a-0150">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A78881-001-a-0160">Majesties</w>
     <w lemma="message" pos="n1" xml:id="A78881-001-a-0170">Message</w>
     <w lemma="send" pos="vvn" xml:id="A78881-001-a-0180">sent</w>
     <w lemma="by" pos="acp" xml:id="A78881-001-a-0190">by</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-0200">the</w>
     <w lemma="lord" pos="n1" xml:id="A78881-001-a-0210">Lord</w>
     <w lemma="chamberlain" pos="n1" xml:id="A78881-001-a-0220">Chamberlain</w>
     <w lemma="to" pos="acp" xml:id="A78881-001-a-0230">to</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-0240">the</w>
     <w lemma="house" pos="n1" xml:id="A78881-001-a-0250">House</w>
     <w lemma="of" pos="acp" xml:id="A78881-001-a-0260">of</w>
     <w lemma="peer" pos="n2" xml:id="A78881-001-a-0270">Peers</w>
     <pc xml:id="A78881-001-a-0280">,</pc>
     <date xml:id="A78881-e100">
      <w lemma="the" pos="d" xml:id="A78881-001-a-0290">the</w>
      <w lemma="28." pos="crd" xml:id="A78881-001-a-0300">28.</w>
      <w lemma="of" pos="acp" xml:id="A78881-001-a-0310">of</w>
      <w lemma="December" pos="nn1" xml:id="A78881-001-a-0320">December</w>
      <pc unit="sentence" xml:id="A78881-001-a-0330">.</pc>
      <w lemma="1641." pos="crd" xml:id="A78881-001-a-0340">1641.</w>
      <pc unit="sentence" xml:id="A78881-001-a-0350"/>
     </date>
    </head>
    <p xml:id="A78881-e110">
     <w lemma="his" pos="po" rend="decorinit" xml:id="A78881-001-a-0360">HIs</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78881-001-a-0370">Majestie</w>
     <w lemma="be" pos="vvg" xml:id="A78881-001-a-0380">being</w>
     <w lemma="very" pos="av" xml:id="A78881-001-a-0390">very</w>
     <w lemma="sensible" pos="j" xml:id="A78881-001-a-0400">sensible</w>
     <w lemma="of" pos="acp" xml:id="A78881-001-a-0410">of</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-0420">the</w>
     <w lemma="great" pos="j" xml:id="A78881-001-a-0430">great</w>
     <w lemma="misery" pos="n2" xml:id="A78881-001-a-0440">Miseries</w>
     <w lemma="and" pos="cc" xml:id="A78881-001-a-0450">and</w>
     <w lemma="distress" pos="n2" xml:id="A78881-001-a-0460">Distresses</w>
     <w lemma="of" pos="acp" xml:id="A78881-001-a-0470">of</w>
     <w lemma="his" pos="po" xml:id="A78881-001-a-0480">His</w>
     <w lemma="subject" pos="n2" xml:id="A78881-001-a-0490">Subjects</w>
     <w lemma="in" pos="acp" xml:id="A78881-001-a-0500">in</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-0510">the</w>
     <w lemma="kingdom" pos="n1" xml:id="A78881-001-a-0520">kingdom</w>
     <w lemma="of" pos="acp" xml:id="A78881-001-a-0530">of</w>
     <w lemma="Ireland" pos="nn1" xml:id="A78881-001-a-0540">Ireland</w>
     <pc xml:id="A78881-001-a-0550">,</pc>
     <w lemma="which" pos="crq" xml:id="A78881-001-a-0560">which</w>
     <w lemma="go" pos="vvb" xml:id="A78881-001-a-0570">go</w>
     <w lemma="daily" pos="av-j" xml:id="A78881-001-a-0580">daily</w>
     <w lemma="increase" pos="vvg" xml:id="A78881-001-a-0590">increasing</w>
     <w lemma="so" pos="av" xml:id="A78881-001-a-0600">so</w>
     <w lemma="fast" pos="av-j" xml:id="A78881-001-a-0610">fast</w>
     <pc xml:id="A78881-001-a-0620">,</pc>
     <w lemma="and" pos="cc" xml:id="A78881-001-a-0630">and</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-0640">the</w>
     <w lemma="blood" pos="n1" xml:id="A78881-001-a-0650">Blood</w>
     <w lemma="which" pos="crq" xml:id="A78881-001-a-0660">which</w>
     <w lemma="have" pos="vvz" xml:id="A78881-001-a-0670">hath</w>
     <w lemma="be" pos="vvn" xml:id="A78881-001-a-0680">been</w>
     <w lemma="already" pos="av" xml:id="A78881-001-a-0690">already</w>
     <w lemma="spill" pos="vvn" reg="spilled" xml:id="A78881-001-a-0700">spilt</w>
     <w lemma="by" pos="acp" xml:id="A78881-001-a-0710">by</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-0720">the</w>
     <w lemma="cruelty" pos="n1" reg="cruelty" xml:id="A78881-001-a-0730">crueltie</w>
     <w lemma="and" pos="cc" xml:id="A78881-001-a-0740">and</w>
     <w lemma="barbarousness" pos="n1" reg="barbarousness" xml:id="A78881-001-a-0750">barbarousnesse</w>
     <w lemma="of" pos="acp" xml:id="A78881-001-a-0760">of</w>
     <w lemma="those" pos="d" xml:id="A78881-001-a-0770">those</w>
     <w lemma="rebel" pos="n2" xml:id="A78881-001-a-0780">Rebels</w>
     <pc xml:id="A78881-001-a-0790">,</pc>
     <w lemma="cry" pos="vvg" xml:id="A78881-001-a-0800">crying</w>
     <w lemma="out" pos="av" xml:id="A78881-001-a-0810">out</w>
     <w lemma="so" pos="av" xml:id="A78881-001-a-0820">so</w>
     <w lemma="loud" pos="j" xml:id="A78881-001-a-0830">loud</w>
     <pc xml:id="A78881-001-a-0840">;</pc>
     <w lemma="and" pos="cc" xml:id="A78881-001-a-0850">and</w>
     <w lemma="perceive" pos="vvg" xml:id="A78881-001-a-0860">perceiving</w>
     <w lemma="how" pos="crq" xml:id="A78881-001-a-0870">how</w>
     <w lemma="slow" pos="av-j" xml:id="A78881-001-a-0880">slowly</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-0890">the</w>
     <w lemma="succour" pos="n2" xml:id="A78881-001-a-0900">Succours</w>
     <w lemma="design" pos="vvd" xml:id="A78881-001-a-0910">designed</w>
     <w lemma="thither" pos="av" xml:id="A78881-001-a-0920">thither</w>
     <w lemma="go" pos="vvi" xml:id="A78881-001-a-0930">go</w>
     <w lemma="on" pos="acp" xml:id="A78881-001-a-0940">on</w>
     <pc xml:id="A78881-001-a-0950">:</pc>
     <w lemma="his" pos="po" xml:id="A78881-001-a-0960">His</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78881-001-a-0970">Majestie</w>
     <w lemma="have" pos="vvz" xml:id="A78881-001-a-0980">hath</w>
     <w lemma="think" pos="vvn" xml:id="A78881-001-a-0990">thought</w>
     <w lemma="fit" pos="j" xml:id="A78881-001-a-1000">fit</w>
     <w lemma="to" pos="prt" xml:id="A78881-001-a-1010">to</w>
     <w lemma="let" pos="vvi" xml:id="A78881-001-a-1020">let</w>
     <w lemma="your" pos="po" xml:id="A78881-001-a-1030">your</w>
     <w lemma="lordship" pos="n2" xml:id="A78881-001-a-1040">Lordships</w>
     <w lemma="know" pos="vvb" xml:id="A78881-001-a-1050">know</w>
     <pc xml:id="A78881-001-a-1060">,</pc>
     <w lemma="and" pos="cc" xml:id="A78881-001-a-1070">and</w>
     <w lemma="desire" pos="vvz" xml:id="A78881-001-a-1080">desires</w>
     <w lemma="you" pos="pn" xml:id="A78881-001-a-1090">you</w>
     <w lemma="to" pos="prt" xml:id="A78881-001-a-1100">to</w>
     <w lemma="acquaint" pos="vvi" xml:id="A78881-001-a-1110">acquaint</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-1120">the</w>
     <w lemma="house" pos="n1" xml:id="A78881-001-a-1130">House</w>
     <w lemma="of" pos="acp" xml:id="A78881-001-a-1140">of</w>
     <w lemma="commons" pos="n2" xml:id="A78881-001-a-1150">Commons</w>
     <w lemma="therewith" pos="av" xml:id="A78881-001-a-1160">therewith</w>
     <pc xml:id="A78881-001-a-1170">,</pc>
     <w lemma="that" pos="cs" xml:id="A78881-001-a-1180">That</w>
     <w lemma="his" pos="po" xml:id="A78881-001-a-1190">His</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A78881-001-a-1200">Majestie</w>
     <w lemma="will" pos="vmb" xml:id="A78881-001-a-1210">will</w>
     <w lemma="take" pos="vvi" xml:id="A78881-001-a-1220">take</w>
     <w lemma="care" pos="n1" xml:id="A78881-001-a-1230">care</w>
     <pc xml:id="A78881-001-a-1240">,</pc>
     <w lemma="that" pos="cs" xml:id="A78881-001-a-1250">that</w>
     <w lemma="by" pos="acp" xml:id="A78881-001-a-1260">by</w>
     <w lemma="commission" pos="n2" xml:id="A78881-001-a-1270">Commissions</w>
     <w lemma="which" pos="crq" xml:id="A78881-001-a-1280">which</w>
     <w lemma="he" pos="pns" xml:id="A78881-001-a-1290">He</w>
     <w lemma="shall" pos="vmb" xml:id="A78881-001-a-1300">shall</w>
     <w lemma="grant" pos="vvi" xml:id="A78881-001-a-1310">grant</w>
     <pc xml:id="A78881-001-a-1320">,</pc>
     <w lemma="ten" pos="crd" xml:id="A78881-001-a-1330">ten</w>
     <w lemma="thousand" pos="crd" xml:id="A78881-001-a-1340">thousand</w>
     <w lemma="english" pos="jnn" xml:id="A78881-001-a-1350">English</w>
     <w lemma="volunteer" pos="n2" reg="volunteers" xml:id="A78881-001-a-1360">Voluntiers</w>
     <w lemma="shall" pos="vmb" xml:id="A78881-001-a-1370">shall</w>
     <w lemma="be" pos="vvi" xml:id="A78881-001-a-1380">be</w>
     <w lemma="speedy" pos="av-j" xml:id="A78881-001-a-1390">speedily</w>
     <w lemma="raise" pos="vvn" xml:id="A78881-001-a-1400">raised</w>
     <w lemma="for" pos="acp" xml:id="A78881-001-a-1410">for</w>
     <w lemma="that" pos="d" xml:id="A78881-001-a-1420">that</w>
     <w lemma="service" pos="n1" xml:id="A78881-001-a-1430">Service</w>
     <pc xml:id="A78881-001-a-1440">,</pc>
     <w lemma="if" pos="cs" xml:id="A78881-001-a-1450">if</w>
     <w lemma="so" pos="av" xml:id="A78881-001-a-1460">so</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-1470">the</w>
     <w lemma="house" pos="n1" xml:id="A78881-001-a-1480">House</w>
     <w lemma="of" pos="acp" xml:id="A78881-001-a-1490">of</w>
     <w lemma="commons" pos="n2" xml:id="A78881-001-a-1500">Commons</w>
     <w lemma="shall" pos="vmb" xml:id="A78881-001-a-1510">shall</w>
     <w lemma="declare" pos="vvi" xml:id="A78881-001-a-1520">declare</w>
     <w lemma="that" pos="cs" xml:id="A78881-001-a-1530">that</w>
     <w lemma="they" pos="pns" xml:id="A78881-001-a-1540">they</w>
     <w lemma="will" pos="vmb" xml:id="A78881-001-a-1550">will</w>
     <w lemma="pay" pos="vvi" xml:id="A78881-001-a-1560">pay</w>
     <w lemma="they" pos="pno" xml:id="A78881-001-a-1570">them</w>
     <pc unit="sentence" xml:id="A78881-001-a-1580">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A78881-e120">
   <div type="colophon" xml:id="A78881-e130">
    <p xml:id="A78881-e140">
     <pc xml:id="A78881-001-a-1590">¶</pc>
     <w lemma="imprint" pos="vvn" xml:id="A78881-001-a-1600">Imprinted</w>
     <w lemma="at" pos="acp" xml:id="A78881-001-a-1610">at</w>
     <w lemma="London" pos="nn1" xml:id="A78881-001-a-1620">London</w>
     <w lemma="by" pos="acp" xml:id="A78881-001-a-1630">by</w>
     <w lemma="Robert" pos="nn1" xml:id="A78881-001-a-1640">Robert</w>
     <w lemma="Barker" pos="nn1" xml:id="A78881-001-a-1650">Barker</w>
     <pc xml:id="A78881-001-a-1660">,</pc>
     <w lemma="printer" pos="n1" xml:id="A78881-001-a-1670">Printer</w>
     <w lemma="to" pos="acp" xml:id="A78881-001-a-1680">to</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-1690">the</w>
     <w lemma="king" pos="n2" xml:id="A78881-001-a-1700">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A78881-001-a-1710">most</w>
     <w lemma="excellent" pos="j" xml:id="A78881-001-a-1720">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A78881-001-a-1730">Majesty</w>
     <pc xml:id="A78881-001-a-1740">:</pc>
     <w lemma="and" pos="cc" xml:id="A78881-001-a-1750">And</w>
     <w lemma="by" pos="acp" xml:id="A78881-001-a-1760">by</w>
     <w lemma="the" pos="d" xml:id="A78881-001-a-1770">the</w>
     <w lemma="assign" pos="vvz" reg="assigns" xml:id="A78881-001-a-1780">Assignes</w>
     <w lemma="of" pos="acp" xml:id="A78881-001-a-1790">of</w>
     <w lemma="JOHN" pos="nn1" xml:id="A78881-001-a-1800">JOHN</w>
     <w lemma="bill" pos="nn1" xml:id="A78881-001-a-1810">BILL</w>
     <pc unit="sentence" xml:id="A78881-001-a-1820">.</pc>
     <w lemma="1641." pos="crd" xml:id="A78881-001-a-1830">1641.</w>
     <pc unit="sentence" xml:id="A78881-001-a-1840"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
